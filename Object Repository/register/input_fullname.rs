<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_fullname</name>
   <tag></tag>
   <elementGuidId>e874bfec-cd2b-4af2-a3f6-8995afffa702</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Nama Lengkap')]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Nama Lengkap')]//input</value>
      <webElementGuid>4a635421-3655-4c41-b799-334c453bf7ec</webElementGuid>
   </webElementProperties>
</WebElementEntity>
