<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_register</name>
   <tag></tag>
   <elementGuidId>b26261fc-6e22-40da-be2b-203aad9e393e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[.//span[contains(.,'Register')]]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[.//span[contains(.,'Register')]]</value>
      <webElementGuid>0bad107a-d7ac-430e-b55e-ce683a66b82e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
