<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_empty_email</name>
   <tag></tag>
   <elementGuidId>5ce29290-ef20-4ca8-9e79-ccf2315e0160</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(., 'email is required') and contains(@class,'v-alert__content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'email is required') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>92065368-4857-45e1-953e-120b7799d5d5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
