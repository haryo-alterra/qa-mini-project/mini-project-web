<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_password</name>
   <tag></tag>
   <elementGuidId>a577a793-a28e-4624-a339-c988ba04b308</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Password')]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Password')]//input</value>
      <webElementGuid>2dd434b8-8b2e-400a-a5e8-167fd0e60c70</webElementGuid>
   </webElementProperties>
</WebElementEntity>
