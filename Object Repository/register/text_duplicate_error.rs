<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_duplicate_error</name>
   <tag></tag>
   <elementGuidId>948a0c6d-6f00-468a-84f4-4d0372984162</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(., 'duplicate key value') and contains(@class,'v-alert__content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'duplicate key value') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>f550e417-7bbd-4cac-a272-37a7a3b06bf2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
