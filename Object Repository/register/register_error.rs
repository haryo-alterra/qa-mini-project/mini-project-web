<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>register_error</name>
   <tag></tag>
   <elementGuidId>4a974c69-6975-4e7c-878d-e4f65a2200e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@role, 'alert')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@role, 'alert')]</value>
      <webElementGuid>b2efd22d-5796-46cc-adaa-9f80685f13fc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
