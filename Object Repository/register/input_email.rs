<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email</name>
   <tag></tag>
   <elementGuidId>12a5d82e-4881-4f79-be40-332c23573b62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Email')]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Email')]//input</value>
      <webElementGuid>90fe85b2-d2bb-4a80-8e74-d0df071d2458</webElementGuid>
   </webElementProperties>
</WebElementEntity>
