<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_empty_fullname</name>
   <tag></tag>
   <elementGuidId>ea706672-404a-4e5d-88d2-c32784ed0182</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(., 'fullname is required') and contains(@class,'v-alert__content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'fullname is required') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>76d0f53f-05bf-483d-80da-f8d2849615f3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
