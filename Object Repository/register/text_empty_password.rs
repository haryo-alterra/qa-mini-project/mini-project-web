<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_empty_password</name>
   <tag></tag>
   <elementGuidId>bdfc2e74-4886-4686-a307-a6d9f4ce8207</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'password is required') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>93a87ef5-707d-40b0-ba8c-ac22fba5d95d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
