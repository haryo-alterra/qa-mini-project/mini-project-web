<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email</name>
   <tag></tag>
   <elementGuidId>7a74d037-edaf-437b-88a0-ef8f0f7d95b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'v-text-field__slot')]//input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Email')]//input</value>
      <webElementGuid>301707ef-ffe2-46cf-b0d1-265e5d814bfb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
