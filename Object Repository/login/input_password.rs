<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_password</name>
   <tag></tag>
   <elementGuidId>6cd5b8b6-ae64-42df-a45b-7d5baae33c92</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Password')]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'v-text-field__slot') and contains(.,'Password')]//input</value>
      <webElementGuid>caca4f1d-4f86-4598-bb10-34aff9b429e1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
