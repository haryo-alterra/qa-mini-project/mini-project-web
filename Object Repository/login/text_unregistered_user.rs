<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_unregistered_user</name>
   <tag></tag>
   <elementGuidId>94a90a34-d111-4e0c-a504-16641a9824a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(., 'record not found') and contains(@class,'v-alert__content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'record not found') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>abafd1eb-ec35-4d4f-a6b6-fc907ed027d6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
