<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_empty_email</name>
   <tag></tag>
   <elementGuidId>2c69e484-71dd-4996-9ef8-28dd4dcaec09</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(., 'email is required') and contains(@class,'v-alert__content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'email is required') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>1fdecab7-be5f-4e60-b1a6-5ea69186e1c6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
