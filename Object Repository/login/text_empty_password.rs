<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_empty_password</name>
   <tag></tag>
   <elementGuidId>6dae6b67-77e4-4cc8-b481-8e32f20a25f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(., 'password is required') and contains(@class,'v-alert__content')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(., 'password is required') and contains(@class,'v-alert__content')]</value>
      <webElementGuid>90b9c778-3dc8-49a3-accd-02330d1000cb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
