<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_next</name>
   <tag></tag>
   <elementGuidId>58946c3c-df0a-467c-a99a-70df98c95931</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@aria-label,'Next page')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@aria-label,'Next page')]</value>
      <webElementGuid>cffbf14b-aee0-4c1c-980e-b7dcdc35f009</webElementGuid>
   </webElementProperties>
</WebElementEntity>
