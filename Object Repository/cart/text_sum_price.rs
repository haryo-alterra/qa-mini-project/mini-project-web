<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_sum_price</name>
   <tag></tag>
   <elementGuidId>faea98b7-7227-4d67-9ec9-e26844f64e9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(@id, 'label-total-bayar')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(@id, 'label-total-bayar')]</value>
      <webElementGuid>a2596a09-a794-4315-a1a0-9fe72e0e8274</webElementGuid>
   </webElementProperties>
</WebElementEntity>
