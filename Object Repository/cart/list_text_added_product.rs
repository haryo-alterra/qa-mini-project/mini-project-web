<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_text_added_product</name>
   <tag></tag>
   <elementGuidId>949dd6fe-1dba-423d-ade1-24e315c2712b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'v-list-item__title label-name')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class, 'v-list-item__title label-name')]</value>
      <webElementGuid>fb1e3196-596d-4129-89d0-9a77b083a580</webElementGuid>
   </webElementProperties>
</WebElementEntity>
