<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_buy</name>
   <tag></tag>
   <elementGuidId>3cf5170e-30fd-49da-bc0a-0547cb90a617</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@id, 'button-bayar')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@id, 'button-bayar')]</value>
      <webElementGuid>7c07329e-9597-4442-a1e4-852fd0b7010e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
