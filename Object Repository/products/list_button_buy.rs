<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_button_buy</name>
   <tag></tag>
   <elementGuidId>93d55244-5b33-4275-b89a-ea06b7af3447</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class, 'button-beli ml-3')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@class, 'button-beli ml-3')]</value>
      <webElementGuid>77f0a086-9641-4d95-8f32-6816e041d9c4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
