<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_text_product_price</name>
   <tag></tag>
   <elementGuidId>0e110e6d-c79f-40c0-bbf8-fb5e5eba8ea2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'product-price')]/b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class, 'product-price')]/b</value>
      <webElementGuid>32b8f8f6-3af2-4e59-8da6-155cc863eeb2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
