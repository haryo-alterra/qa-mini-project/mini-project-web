<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_cart</name>
   <tag></tag>
   <elementGuidId>cc8ba835-a750-4d8c-a10b-1980517d67fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'v-btn v-btn--icon v-btn--round theme--dark v-size--default'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class = 'v-btn v-btn--icon v-btn--round theme--dark v-size--default'][1]</value>
      <webElementGuid>3f9149e6-60f5-4df2-8a81-6cec6aebd605</webElementGuid>
   </webElementProperties>
</WebElementEntity>
