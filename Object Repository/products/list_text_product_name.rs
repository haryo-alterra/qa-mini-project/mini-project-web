<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_text_product_name</name>
   <tag></tag>
   <elementGuidId>94a92f40-c9c2-4910-a6a5-686b2389101a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class, 'v-card__title product-title')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class, 'v-card__title product-title')]</value>
      <webElementGuid>866d557f-90e4-45bc-b46f-5f707067258b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
