<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_login</name>
   <tag></tag>
   <elementGuidId>0a1bb2c1-8437-42e0-95c7-b947f9ee38df</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'v-btn v-btn--icon v-btn--round theme--dark v-size--default'][2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class = 'v-btn v-btn--icon v-btn--round theme--dark v-size--default'][2]</value>
      <webElementGuid>cfa971a1-c2b9-4e76-adaf-427abca34a32</webElementGuid>
   </webElementProperties>
</WebElementEntity>
