<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_product_title</name>
   <tag></tag>
   <elementGuidId>06e11360-8384-422b-88ad-cc21cff9df12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[contains(@class, 'text-h4')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[contains(@class, 'text-h4')]</value>
      <webElementGuid>90380c48-f2e4-4b1f-8536-e519d94f472f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
