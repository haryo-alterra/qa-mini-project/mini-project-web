import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement

List<WebElement> buttonElements = WebUI.findWebElements(
	findTestObject('Object Repository/products/list_button_buy'), GlobalVariable.timeout)
List<WebElement> productNameElements = WebUI.findWebElements(
	findTestObject('Object Repository/products/list_text_product_name'), GlobalVariable.timeout)
List<WebElement> productPriceElements = WebUI.findWebElements(
	findTestObject('Object Repository/products/list_text_product_price'), GlobalVariable.timeout)

List<String> listOfProductName = new ArrayList<>()
List<Integer> listOfProductPrice = new ArrayList<>()

for (int i = 0; i < total; i++) {
	String name = productNameElements.get(i).getText()
	String moneyValue = productPriceElements.get(i).getText()
	moneyValue = moneyValue.replace(".", "")
	int price = Integer.parseInt(moneyValue)
	listOfProductName.add(name)
	listOfProductPrice.add(price)
	buttonElements.get(i).click()
}

return [listOfProductName, listOfProductPrice]