import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(GlobalVariable.timeout_fast)
WebUI.back()

int idx = 1
for (int i = 1; i <= product_list.size(); i++) {
	String name = WebUI.getText(findTestObject(
		'Object Repository/transaction/list_text_product',
		[('index'):idx, ('column'):1]))
	int price = Integer.parseInt(WebUI.getText(findTestObject(
		'Object Repository/transaction/list_text_product',
		[('index'):idx, ('column'):4])))
	println name
	println price
	WS.verifyEqual(name, product_list.get(i-1))
	WS.verifyEqual(price, price_list.get(i-1))
	
	idx++
	if (i % 10 == 0 && i > 0) {
		WebUI.click(findTestObject('Object Repository/transaction/button_next'))
		idx = 1
	}
}