package products
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class add_products {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	List<String> listOfProductNames = new ArrayList<>()
	List<Integer> listOfProductPrice = new ArrayList<>()
	int amountIncrement = 0

	@When("I add the first (\\d+) products listed")
	def I_add_the_first_n_products_listed(int num) {
		def object = WebUI.callTestCase(
				findTestCase('_pages/products/add_multiple_products'),
				[('total'): num], FailureHandling.STOP_ON_FAILURE)
		listOfProductNames = object[0]
		listOfProductPrice = object[1]
	}

	@And("I add the first (\\d+) products to buy")
	def I_add_the_first_n_products_to_buy(int num) {
		def object = WebUI.callTestCase(
				findTestCase('_pages/products/add_multiple_products'),
				[('total'): num], FailureHandling.STOP_ON_FAILURE)
		listOfProductNames = object[0]
		listOfProductPrice = object[1]
	}

	@And("I click the cart button")
	def I_click_the_cart_button() {
		WebUI.callTestCase(
				findTestCase('_pages/products/click_cart_button'),
				null, FailureHandling.STOP_ON_FAILURE)
	}

	@And("I add all products amount by (\\d+)")
	def I_add_all_products_amount_by(int num) {
		amountIncrement = num
		WebUI.callTestCase(
				findTestCase('_pages/cart/add_product_amount'),
				[('total_click'):amountIncrement], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I click the buy button")
	def I_click_the_buy_button() {
		WebUI.callTestCase(
				findTestCase('_pages/cart/click_buy_button'),
				null, FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify if all products are added in cart")
	def I_verify_if_all_products_are_added_in_cart() {
		WebUI.callTestCase(
				findTestCase('_pages/cart/verify_all_added_products'),
				[('price_sum'): listOfProductPrice.sum(),
					('product_list'): listOfProductNames],
				FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify if all bought products are listed in transaction")
	def I_verify_all_bought_products_in_transaction() {
		WebUI.callTestCase(
				findTestCase('_pages/transaction/verify_saved_transactions'),
				[('price_list'): listOfProductPrice,
					('product_list'): listOfProductNames],
				FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("I verify the resulting product amount in transaction")
	def I_verify_the_resulting_product_amount_in_transaction() {
		WebUI.callTestCase(
			findTestCase('_pages/transaction/verify_products_amount'),
			[('amount'): amountIncrement+1,
				('product_list'): listOfProductNames],
			FailureHandling.STOP_ON_FAILURE)
	}
}