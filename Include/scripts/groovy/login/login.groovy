package login
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import common.CommonLoginSteps
import common.Randomizer


class login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	String email = ""
	String password = ""

	@When("I click login icon")
	def I_click_login_icon() {
		CommonLoginSteps.goToLoginPage()
	}

	@And("I enter email (.*) and password (.*)")
	def I_enter_email_and_password(String email, String password) {
		this.email = email
		this.password = password
		CommonLoginSteps.login(email, password)
	}

	@And("I enter random unregistered email and password")
	def I_enter_random_unregistered_email_and_password() {
		this.email = Randomizer.randomizeString(10) + "@mail.com"
		this.password = Randomizer.randomizeString(10)
		CommonLoginSteps.login(email, password)
	}

	@And("I click login button")
	def I_click_login_button() {
		CommonLoginSteps.clickLogin()
	}

	@Then("I check if empty login input messages appear")
	def I_check_if_empty_login_input_messages_appear() {
		if (email.length() == 0) {
			WebUI.callTestCase(
					findTestCase('_pages/login/verify_empty_email'), null,
					FailureHandling.STOP_ON_FAILURE)
			return
		}
		if (password.length() == 0) {
			WebUI.callTestCase(
					findTestCase('_pages/login/verify_empty_password'), null,
					FailureHandling.STOP_ON_FAILURE)
		}
	}

	@Then("I check if invalid login credentials message appears")
	def I_check_if_invalid_login_credentials_message_appears() {
		WebUI.callTestCase(findTestCase('_pages/login/verify_unregistered_login'),
				null,
				FailureHandling.STOP_ON_FAILURE)
	}
}