#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@products
Feature: Products Feature
  I want to be able to manage products listed in the app
  
  Background:
  	Given I open the page

  @add_products
  Scenario: I want to buy products and see the selected ones in my cart before buying
    When I add the first 13 products listed
    And I click the cart button
    Then I verify if all products are added in cart
  
  @check_detail
  Scenario Outline: I want to check detail of listed products
  	When I click detail on product number <index>
  	Then I verify if product detail page shows the right product
  	
  	Examples:
  	| index |
  	| 4 |
  	| 100 |
  	| 666 |
  	
  	