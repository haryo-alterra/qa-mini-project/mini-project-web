#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@login
Feature: Login Feature
  I want to login as a user
  
  Background:
  	Given I open the page
  	When I click login icon
  	
  @invalid_login
  Scenario: I should not be able to log in with unregistered data
  	And I enter random unregistered email and password
  	And I click login button
  	Then I check if invalid login credentials message appears

  @empty_login
  Scenario Outline: I should not be able to log in with empty data
    And I enter email <email> and password <password>
    And I click login button
    Then I check if empty login input messages appear

    Examples: 
      | email  | password |
      | | abcd1234 |
      | h@gmail.com | |
      | | |
  
  @register_and_login
  Scenario: I want to be able to register to use the app
  	And I go to register page
  	And I register with random unregistered email and password and Test 123 as my full name
  	And I click register
  	And I login with the newly-created user
  	Then I check if I can login with that newly registered account