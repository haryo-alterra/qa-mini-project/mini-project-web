#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@transaction
Feature: Transaction Feature
  I want to be able to buy products available
  
  Background:
  	Given I open the page
  	When I click login icon
  	And I go to register page
  	And I register with random unregistered email and password and Test 123 as my full name
  	And I click register
  	And I login with the newly-created user

  @buy_products
  Scenario: I want to be able to buy products listed if I have registered as a user
  	And I add the first 21 products to buy
  	And I click the cart button
  	And I click the buy button
  	Then I verify if all bought products are listed in transaction
  
  @edit_products_amount
  Scenario: I want to edit the amount of products I buy
  	And I add the first 4 products to buy
  	And I click the cart button
  	And I add all products amount by 2
  	And I click the buy button
  	Then I verify the resulting product amount in transaction