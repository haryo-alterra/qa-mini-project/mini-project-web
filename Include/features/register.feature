#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@register
Feature: Register Feature
  I want to be able to register as a new user
  
  Background:
  	Given I open the page
  	When I click login icon
  	And I go to register page
  
  @same_email_register
  Scenario: I should not be able to register with already used email
  	And I register with random unregistered email and password and Test 123 as my full name
  	And I click register
  	And I register with the same email
  	Then I check if registration process results in error

  @empty_register
  Scenario Outline: I should not be able to register with empty data
    And I enter email <email>, password <password> with <fullname> as my full name
    And I click register
    Then I check if registration process results in error

    Examples: 
      | email | password | fullname  |
      | | abcd1234 | Makmur bin Sukses |
      | abc@mail.com | abcd1234 | |
      | abc@mail.com | | Makmur bin Sukses |
      | | | |